const Joi = require("@hapi/joi");

var user_schema = Joi.object({
  name: Joi.string().required(),
  email: Joi.string().email().lowercase().required(),
  password: Joi.string().min(2).required(),
  adress: Joi.string().required(),
  token: Joi.string(),
});
var product_schema = Joi.object({
  name: Joi.string().required(),
  price: Joi.number().required(),
  //quantity: Joi.number().min(1).required(),
  availabillity: Joi.string(),
});

module.exports = {
  user_schema,
  product_schema,
};
