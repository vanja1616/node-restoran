const mongoose = require("mongoose");

var products_schema = new mongoose.Schema({
    name: {
      type: String,
      required: true,
    },
    price: {
      type: Number,
      required: true,
    },
    quantity: {
      type: Number,
      //required: true,
      min: [1, "Quantity can nott be less then one"],
    },
    availabillity: String,
  });
  const Productdb = mongoose.model("productdb", products_schema);
  module.exports = {
    Productdb
  };
  