const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const { user_schema } = require("../database/model/validation");
const { Userdb } = require("../database/model/model");

exports.create = async (req, res) => {
  // validate request
  try {
    if (!req.body) {
      res.status(400).send({ message: "Content can not be emtpy!" });
      return;
    }

    const { name, email, password, adress } = req.body;
    const result = await user_schema.validateAsync(req.body);

    const oldUser = await Userdb.findOne({ email: email });
    if (oldUser) {
      return res.status(409).send("User already exist");
    }
    encryptedPassword = await bcrypt.hash(password, 10);
    // new user
    const user = await Userdb.create({
      name,
      adress,
      email: email.toLowerCase(),
      password: encryptedPassword,
    });
    const token = jwt.sign(
      {
        user_id: user._id,
        email,
      },
      "sadasdasd",
      {
        expiresIn: "2h",
      }
    );
    user.token = token;
    res.status(201).json(user);

    res.redirect("/products");
    res.send(data);
  } catch (err) {
    console.log(err);
  }
}
  exports.find = (req, res) => {
    if (req.query.id) {
      const id = req.query.id;

      Userdb.findById(id)
        .then((data) => {
          if (!data) {
            res.status(404).send({ message: "Not found user with id " + id });
          } else {
            res.send(data);
          }
        })
        .catch((err) => {
          res
            .status(500)
            .send({ message: "Erro retrieving user with id " + id });
        });
    } else {
      Userdb.find()
        .then((user) => {
          res.send(user);
        })
        .catch((err) => {
          res.status(500).send({
            message:
              err.message || "Error Occurred while retriving user information",
          });
        });
    }
  };

  // Update a new idetified user by user id
  exports.update = (req, res) => {
    if (!req.body) {
      return res
        .status(400)
        .send({ message: "Data to update can not be empty" });
    }

    const id = req.params.id;
    Userdb.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
      .then((data) => {
        if (!data) {
          res.status(404).send({
            message: `Cannot Update user with ${id}. Maybe user not found!`,
          });
        } else {
          res.send(data);
        }
      })
      .catch((err) => {
        res.status(500).send({ message: "Error Update user information" });
      });
  };

  // Delete a user with specified user id in the request
  exports.delete = (req, res) => {
    const id = req.params.id;

    Userdb.findByIdAndDelete(id)
      .then((data) => {
        if (!data) {
          res.status(404).send({
            message: `Cannot Delete with id ${id}. Maybe id is wrong`,
          });
        } else {
          res.send({
            message: "User was deleted successfully!",
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Could not delete User with id=" + id,
        });
      });
  };

module.exports.users = require("./products");
