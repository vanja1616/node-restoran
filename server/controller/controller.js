const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const { user_schema } = require("../database/model/validation");

const { Userdb } = require("../database/model/model");
// create and save new user
exports.login = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    //const result = await user_schema.validateAsync(req.body);
    const user = await Userdb.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        {
          user_id: user._id,
          email,
        },
        "sadasdasd",
        {
          expiresIn: "2h",
        }
      );
      user.token = token;
      res.redirect("/products");
      //res.status(200).json(user);
      //res.send(data);
      //res.redirect("/users");
    }
    res.status(400).send("Invalid credentials");
  } catch (err) {
    console.log(err);
  }
};
module.exports.products = require("./products");
module.exports.users = require("./products");
