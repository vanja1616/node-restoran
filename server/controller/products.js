const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
require("dotenv").config();
const { product_schema } = require("../database/model/validation");
const { Productdb } = require("../database/model/products_model");

exports.createProduct = (req, res) => {
  if (!req.body) {
    res.status(400).send({ message: "Content cannot be emited" });
    return;
  }

  const product = new Productdb({
    name: req.body.name,
    price: req.body.price,
    availabillity: req.body.availabillity,
  });
  product
    .save(product)
    .then((data) => {
      res.redirect("/add-product");
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some errors occured",
      });
    });
};

//return and retrieve all products/one product
exports.findProduct = (req, res) => {
  if (req.query.id) {
    const id = req.query.id;

    Productdb.findById(id)
      .then((data) => {
        if (!data) {
          res.status(404).send({ message: "User not found" });
        } else {
          res.send(data);
        }
      })
      .catch((err) => {
        res.status(500).send({ message: "Error ocucred" });
      });
  } else {
    Productdb.find()
      .then((product) => {
        res.send(product);
      })
      .catch((err) => {
        res.status(500).send({ message: err.message || "Error occured" });
      });
  }
};

exports.updateProduct = (req, res) => {
  if (!req.body) {
    return res.status(400).send({ message: "Data to update can't be empty" });
  }

  const id = req.params.id;
  Productdb.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then((data) => {
      if (!data) {
        res.status(404).send({ message: "Cannot Update" });
      } else {
        res.send(data);
      }
    })
    .catch((err) => {
      res.status(500).send({ message: "Error Update product" });
    });
};
exports.deleteProduct = (req, res) => {
  const id = req.params.id;
  Productdb.findByIdAndDelete(id)
    .then((data) => {
      if (!data) {
        res.status(404).send({ message: "Cannot delete product" });
      } else {
        res.send({
          message: "Product deleted successfully",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "could not delete product",
      });
    });
};
